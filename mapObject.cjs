function mapObject(object, cb) {
    let obj = {};
    if(typeof object !== 'object')
    {
        return {};
    }
    for (key in object) {
        obj[key] = cb(key, object);
    }
    return obj;
}
module.exports = mapObject;