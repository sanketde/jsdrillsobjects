function invert(obj) {
    let invertedObj = {};

    if(typeof obj !== 'object')
    {
        return invertedObj;
    }

    for (key in obj) {
        invertedObj[obj[key]] = key;
    }
    return invertedObj;
}
module.exports = invert;