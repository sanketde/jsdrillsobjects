let defaults = require('../defaults.cjs')

const testObject = { name: 'Bruce Wayne', age: undefined, location: 'Gotham' };
const defaultProps = { age: 'NA', location: 'NA' };

object = defaults(testObject, defaultProps);
console.log(object);