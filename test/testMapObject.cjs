let mapObject = require('../mapObject.cjs')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

valuesArray = mapObject(testObject, (key, object) => {
    if (typeof object[key] === 'string') {
        return object[key].toUpperCase();
    }
    else {
        return object[key] + 5;
    }
});
console.log(valuesArray);