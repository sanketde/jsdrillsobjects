function keys(object) {
    let keysArray = [];

    if(typeof object !== 'object')
    {
        return {};
    }
    for (let key in object) {
        keysArray.push(key);
    }
    return keysArray;
}
module.exports = keys;
