function defaults(obj, defaultProps) {
    let copyObject = {};

    if(typeof obj !== 'object' || defaultProps === undefined)
    {
        return copyObject;
    }

    for (key in obj) {
        if (obj[key] === undefined) {
            copyObject[key] = defaultProps[key];
        }
        else {
            copyObject[key] = obj[key];
        }

    }
    return copyObject;
}
module.exports = defaults;