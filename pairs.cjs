function pairs(obj) {
    let pairsArray = [];
    if(typeof obj !== 'object')
    {
        return {};
    }
    for (key in obj) {
        pairsArray.push([key,obj[key]])
    }
    return pairsArray;
}
module.exports = pairs;